#define _USE_MATH_DEFINES
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <thread>

#include <vector>

#if defined(__APPLE__)
#include <GLUT/GLUT.h>
#include <OpenGL/gl3.h>
#include <OpenGL/glu.h>
#else
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#include <windows.h>
#endif
#include <GL/glew.h>		// must be downloaded 
#include <GL/freeglut.h>	// must be downloaded unless you have an Apple
#endif

const unsigned int windowWidth = 800, windowHeight = 800;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// OpenGL major and minor versions

using namespace std;
#define epsilon 0.01
#define MAX_DEPTH 6
#define NUM_THREADS 16

int majorVersion = 3, minorVersion = 0;

void getErrorInfo(unsigned int handle) {
	int logLen;
	glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logLen);
	if (logLen > 0) {
		char * log = new char[logLen];
		int written;
		glGetShaderInfoLog(handle, logLen, &written, log);
		printf("Shader log:\n%s", log);
		delete log;
	}
}

// check if shader could be compiled
void checkShader(unsigned int shader, char * message) {
	int OK;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &OK);
	if (!OK) {
		printf("%s!\n", message);
		getErrorInfo(shader);
	}
}

// check if shader could be linked
void checkLinking(unsigned int program) {
	int OK;
	glGetProgramiv(program, GL_LINK_STATUS, &OK);
	if (!OK) {
		printf("Failed to link shader program!\n");
		getErrorInfo(program);
	}
}

// vertex shader in GLSL
const char *vertexSource = R"(
	#version 130
    precision highp float;

	in vec2 vertexPosition;		// variable input from Attrib Array selected by glBindAttribLocation
	out vec2 texcoord;			// output attribute: texture coordinate

	void main() {
		texcoord = (vertexPosition + vec2(1, 1))/2;							// -1,1 to 0,1
		gl_Position = vec4(vertexPosition.x, vertexPosition.y, 0, 1); 		// transform to clipping space
	}
)";

// fragment shader in GLSL
const char *fragmentSource = R"(
	#version 130
    precision highp float;

	uniform sampler2D textureUnit;
	in  vec2 texcoord;			// interpolated texture coordinates
	out vec4 fragmentColor;		// output that goes to the raster memory as told by glBindFragDataLocation

	void main() {
		fragmentColor = texture(textureUnit, texcoord); 
	}
)";

// row-major matrix 4x4
struct mat4 {
	float m[4][4];
public:
	mat4() {}
	mat4(float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23,
		float m30, float m31, float m32, float m33) {
		m[0][0] = m00; m[0][1] = m01; m[0][2] = m02; m[0][3] = m03;
		m[1][0] = m10; m[1][1] = m11; m[1][2] = m12; m[1][3] = m13;
		m[2][0] = m20; m[2][1] = m21; m[2][2] = m22; m[2][3] = m23;
		m[3][0] = m30; m[3][1] = m31; m[3][2] = m32; m[3][3] = m33;
	}

	mat4 operator*(const mat4& right) {
		mat4 result;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				result.m[i][j] = 0;
				for (int k = 0; k < 4; k++) result.m[i][j] += m[i][k] * right.m[k][j];
			}
		}
		return result;
	}
	operator float*() { return &m[0][0]; }
};


// 3D point in homogeneous coordinates
struct vec4 {
	float v[4];

	vec4(float x = 0, float y = 0, float z = 0, float w = 1) {
		v[0] = x; v[1] = y; v[2] = z; v[3] = w;
	}

	vec4 operator*(const mat4& mat) {
		vec4 result;
		for (int j = 0; j < 4; j++) {
			result.v[j] = 0;
			for (int i = 0; i < 4; i++) result.v[j] += v[i] * mat.m[i][j];
		}
		return result;
	}

	vec4 operator+ (const vec4& vec) const{
	 return vec4(v[0] + vec.v[0], v[1] + vec.v[1], v[2] + vec.v[2],v[3] + vec.v[3] );
	}

	vec4 operator+ (float m) const{
	 return vec4(v[0] + m, v[1] + m, v[2] + m, v[3] + m);
	}

	vec4 operator+= (const vec4& vec) {
	  *this = *this + vec;
	  return *this;
	}

	vec4 operator- (const vec4& vec) const{
	  return vec4(v[0]-vec.v[0], v[1] - vec.v[1], v[2]- vec.v[2],v[3] - vec.v[3] );
	} 

	vec4 operator- (float m) const{
	  return vec4(v[0] - m, v[1] - m, v[2] - m, v[3] - m );
	}
	

	vec4 operator-= (vec4& vec) {
	  *this = *this - vec;
	  return *this;
	}

	vec4 operator* (vec4& vec) {
	 return vec4(v[0]*vec.v[0], v[1] * vec.v[1], v[2]* vec.v[2],v[3] * vec.v[3] );
	} 

	vec4 operator*= (vec4& vec) {
	  *this = *this * vec;
	  return *this;
	}

	vec4 operator* (float m) const{
	  return vec4(v[0] * m, v[1] * m, v[2] * m, v[3] * m );
	}

	vec4 operator*= (float m) {
	  *this = *this * m;
	  return *this;
	}

	vec4 operator/ (const vec4& vec) const{
	 return vec4(v[0]/vec.v[0], v[1] / vec.v[1], v[2]/ vec.v[2], v[3] / vec.v[3] );
	} 


	vec4 operator/ (float d) const{
	 return vec4(v[0]/d, v[1] / d, v[2]/d, v[3] / d );
	}

	vec4 cross(const vec4& vec) const{
		return vec4(v[1]*vec.v[2] - v[2]*vec.v[1], 
					v[2]*vec.v[0] - v[0]*vec.v[2],
					v[0]*vec.v[1] - v[1]*vec.v[0],
					1);
	}

	float dot(vec4& vec) const{
		return v[0]*vec.v[0] + v[1]*vec.v[1] + v[2]*vec.v[2];
	}

	float length() const{
		return sqrtf(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
	}

	float sum(){
		return (v[0] + v[1] + v[2]);
	}

	vec4 normalize() const{
		vec4 result = *this / this->length();
		return result;
	}

	vec4 sin(float m){
		return vec4(sinf(v[0]), sinf(v[1]), sinf(v[2]), sinf(v[3]));
	}

	vec4 cos(float m){
		return vec4(cosf(v[0]), cosf(v[1]), cosf(v[2]), cosf(v[3]));
	}

};

float dot(const vec4& a ,const vec4& b){
		return a.v[0]*b.v[0] + a.v[1]*b.v[1] + a.v[2]*b.v[2];
	}

vec4 operator* (const vec4& rhs, const vec4& lhs) {
	return vec4(rhs.v[0]*lhs.v[0], rhs.v[1]*lhs.v[1], rhs.v[2]*lhs.v[2], rhs.v[3]*lhs.v[3]);
} 

int sign(float val) {
	return ((0.0f) < val) - (val < (0.0f));
}


// handle of the shader program
unsigned int shaderProgram;

class FullScreenTexturedQuad {
	unsigned int vao, textureId;	// vertex array object id and texture id
public:
	void Create(vec4 image[windowWidth * windowHeight]) {
		glGenVertexArrays(1, &vao);	// create 1 vertex array object
		glBindVertexArray(vao);		// make it active

		unsigned int vbo;		// vertex buffer objects
		glGenBuffers(1, &vbo);	// Generate 1 vertex buffer objects

		// vertex coordinates: vbo[0] -> Attrib Array 0 -> vertexPosition of the vertex shader
		glBindBuffer(GL_ARRAY_BUFFER, vbo); // make it active, it is an array
		static float vertexCoords[] = { -1, -1,   1, -1,  -1, 1, 
			                             1, -1,   1,  1,  -1, 1 };	// two triangles forming a quad
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexCoords), vertexCoords, GL_STATIC_DRAW);	   // copy to that part of the memory which is not modified 
		// Map Attribute Array 0 to the current bound vertex buffer (vbo[0])
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,	0, NULL);     // stride and offset: it is tightly packed

		// Create objects by setting up their vertex data on the GPU
		glGenTextures(1, &textureId);  				// id generation
		glBindTexture(GL_TEXTURE_2D, textureId);    // binding

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, windowWidth, windowHeight, 0, GL_RGBA, GL_FLOAT, image); // To GPU
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); // sampling
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}

	void Draw() {
		glBindVertexArray(vao);	// make the vao and its vbos active playing the role of the data source
		int location = glGetUniformLocation(shaderProgram, "textureUnit");
		if (location >= 0) {
			glUniform1i(location, 0);		// texture sampling unit is TEXTURE0
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, textureId);	// connect the texture to the sampler
		}
		glDrawArrays(GL_TRIANGLES, 0, 6);	// draw two triangles forming a quad
	}
};

// The virtual world: single quad
FullScreenTexturedQuad fullScreenTexturedQuad;

struct Ray{
	vec4 ps;
	vec4 nm;
	Ray(vec4 _ps, vec4 _nm){
		ps = _ps;
		nm = _nm.normalize();
	}
};

class Camera
{
private:
	vec4 eye;
	vec4 lookAt;
	vec4 up;
	vec4 right;
public:
	 Camera(vec4 eye, vec4 lookAt, vec4 up, vec4 right){
	 		this->eye = eye;
	 		this->lookAt = lookAt;
	 		this->up = up;
	 		this->right = right;
	 }

	 Ray getRay(int x, int y){
	 	vec4 nm = lookAt + right * (float(x) / windowWidth - 0.5f) +
	 				up * (float(y) / windowHeight - 0.5f);
	 			
	 	return Ray(eye, nm);
	 }	
};

struct Material{
	vec4 F0, ka, kd, ks; 
	bool isReflective, isRefractive;
	float shininess;
	vec4  k, n;	

	Material(vec4 _k, vec4 _n){
		k = _k;
		n = _n;
		F0 = ((n-1.0f) * (n-1.0f) + k * k) / ((n+1.0f) * (n+1.0f) + k * k );
	}

    vec4 shade( const vec4& normal, const vec4& viewDir, const vec4& lightDir,
              const vec4& inRad) 
	{
		vec4 reflRad(0, 0, 0, 0);
		float cosTheta = dot(normal, lightDir);
		if(cosTheta < 0) return reflRad;
		reflRad = inRad * kd * cosTheta;;
		vec4 halfway = (viewDir + lightDir).normalize();
		float cosDelta = dot(normal, halfway);
		if(cosDelta < 0) return reflRad;
		return reflRad + inRad * ks * pow(cosDelta,shininess);
	}

	vec4 Fresnel(const vec4& inDir, const vec4& normal) {
		float cosa = fabs(dot(normal, inDir));
		return F0 + (vec4(1, 1, 1, 1) - F0) * pow(1-cosa, 5);
	}


	vec4 reflect(const vec4& inDir, const vec4& normal) {
		return (inDir - normal * dot(normal, inDir) * 2.0f);
	}

	vec4 refract(const vec4& inDir, vec4 normal) {
		//TODO n.v
		float ior = n.v[0];
		float cosa = dot(normal, inDir) * -1.0f;
		if (cosa < 0.0f) { cosa *= -1.0f; normal *= -1.0f; ior = 1 / n.v[0]; }      
		float disc = 1.0f - (1.0f - cosa * cosa) / (ior * ior);       
		if (disc < 0.0f) return reflect(inDir, normal);
		return (inDir / ior) + normal * (cosa / ior - sqrtf(disc));
	}
};



struct Hit {
  float t;
  vec4 ps;
  vec4 nm;
  Material* material;
  Hit() { t = -1; }
};



struct Intersectable
{
  Material* material;
  virtual ~Intersectable() {}
  virtual Hit intersect(const Ray& ray)=0;
};


class Sphere : public Intersectable
{
public:
	vec4 ps;
	float ri; 


	Sphere(vec4 p, float r, Material* material)
	{
		ps = p;
		ri = r;
		this->material = material;
	}
	Hit intersect(const Ray& ray) {
		Hit hit;

		float a = dot(ray.nm, ray.nm);
		float b = dot(ray.nm, (ray.ps - ps)) * 2;
		float c = dot((ray.ps - ps), (ray.ps - ps)) - ri*ri; 
		float d = b * b - 4 * a * c;
		if(d < 0)
			return hit;
		float t1 = ((-1.0 * b - sqrtf(d)) / (2.0 * a));
		float t2 = ((-1.0 * b + sqrtf(d)) / (2.0 * a));
		if(0 < t1 && 0 < t2)
			hit.t = t1;
		if( t1 < 0)
			hit.t = t2;
	
		hit.ps = ray.ps + (ray.nm * hit.t);
		hit.nm = ( hit.ps - ps ).normalize();
		hit.material = this->material;

		return hit; 
	}
};


class Triangle : public Intersectable{
private:
	vec4 r1;
	vec4 r2;
	vec4 r3;
	vec4 nm;
public:
	Triangle(vec4 _r1, vec4 _r2, vec4 _r3, Material* material){
		r1 = _r1;
		r2 = _r2;
		r3 = _r3;
		nm = (r2 - r1).cross(r3 - r1).normalize();
		this->material = material;
	}

	~Triangle(){}

	Hit intersect(const Ray& ray) {
		Hit hit;
		float t = dot(r1 - ray.ps, nm) / dot(ray.nm, nm);
		if (t < 0.0f || isnan(t)) 
			return hit;
		
		hit.ps = ray.ps + ray.nm * t;
		
		if (dot((r2 - r1).cross(hit.ps - r1), nm) > 0 &&
			dot((r3 - r2).cross(hit.ps - r2), nm) > 0 &&
			dot((r1 - r3).cross(hit.ps - r3), nm) > 0) {
			hit.material = material;
			hit.nm = nm;
			hit.t = t;
		}
		return hit;
	}
};

class Rectangle : public Intersectable
{
public:
	Triangle* triangle1;
	Triangle* triangle2;

	Rectangle(vec4 ps, vec4 up, vec4 right, float a, float b, Material* material)
	{
		vec4 p1 = ps - up * (b/2) - right * (a/2);

		vec4 p2 = ps - up * (b/2) + right * (a/2);
		vec4 p2_ = ps - up * (b/1.999) + right * (a/2.001);

		vec4 p3_ = ps + up * (b/2.001) - right * (a/1.999);
		vec4 p3 = ps + up * (b/2) - right * (a/2);

		vec4 p4 = ps + up * (b/2) + right * (a/2);

		triangle1 = new Triangle(p1, p2, p3, material);
		triangle2 = new Triangle(p2_, p4, p3_, material);
	}
	~Rectangle(){
		delete triangle1;
		delete triangle2;
	}
	Hit intersect(const Ray& ray) {
		Hit hit1, hit2;
		hit1 = triangle1->intersect(ray);
		hit2 = triangle2->intersect(ray);
		if(hit1.t < hit2.t)
			return hit2;
		else
			return hit1;
	}
};


class Cuboid : public Intersectable
{
public:
	Rectangle** rectangle;
	Cuboid(vec4 ps, vec4 up, vec4 right, float a, float b, float c, Material* material)
	{		
		vec4 front = (right).cross(up).normalize();

		rectangle = new Rectangle*[6];
		rectangle[0] = new Rectangle(ps+front*(b/2)*0.9999, up, right, a, c, material);
		rectangle[1] = new Rectangle(ps+right*(a/2)*0.9999, up, front * -1.0f, b, c, material);
		rectangle[2] = new Rectangle(ps-front*(b/2)*0.9999, up, right * -1.0f, a, c, material);
		rectangle[3] = new Rectangle(ps-right*(a/2)*0.9999, up, front, b, c , material);
		rectangle[4] = new Rectangle(ps-up*(c/2)*0.9999, front, right , a, b, material);
		rectangle[5] = new Rectangle(ps+up*(c/2)*0.9999, front, right * -1.0f, a, b, material);
	}
	~Cuboid(){
		for(int i = 0; i<6; i++)
			delete rectangle[i];
		delete rectangle;
	}
	Hit intersect(const Ray& ray) {
		Hit hit, bestHit;

		for(int i = 0; i<6; i++){
			hit = rectangle[i]->intersect(ray);
			if(hit.t > 0 && (bestHit.t < 0 || hit.t < bestHit.t))
				bestHit = hit;
		}
		return bestHit;
	}
};


struct Water : public Intersectable {
public:
	Cuboid* cuboid;
	vec4 waveStart;

	Water(vec4 ps, vec4 up, vec4 right, float a, float b, float c, vec4 waveStart, Material* material) {
		cuboid = new Cuboid(ps, up, right, a, b, c, material);
		this->waveStart = waveStart;

	}
	~Water(){
		delete cuboid;
	}

	Hit intersect(const Ray& ray) {
		Hit hit = cuboid->intersect(ray);
		Hit topHit = cuboid->rectangle[5]->intersect(ray);

		if( hit.t - epsilon > topHit.t &&  topHit.t < hit.t+ epsilon)
			return hit;

		float A = 0.5;
		float omega = 6;
		float distance = sqrtf((waveStart.v[0] - topHit.ps.v[0])*(waveStart.v[0] - topHit.ps.v[0])+
							   (waveStart.v[1] - topHit.ps.v[1])*(waveStart.v[1] - topHit.ps.v[1]));
		float h = A * cosf(omega * distance);
		float tanx = -A*omega*(waveStart.v[0] - topHit.ps.v[0])*(waveStart.v[0] - topHit.ps.v[0]) * sin(omega*distance) / distance;
		float tany = -A*omega*(waveStart.v[1] - topHit.ps.v[1])*(waveStart.v[1] - topHit.ps.v[1]) * sin(omega*distance) / distance;
		hit.ps.v[2] +=  h;
		hit.nm = vec4(1,0,tanx,0).cross(vec4(0,1,tany,0)).normalize();

		return hit;
	}
};


struct Light{
	vec4 color;
	vec4 ps;
	
};

vector<Intersectable*> objects;
vector<Light*> lights;


Hit firstIntersect(Ray ray) {
  Hit bestHit;
  for(unsigned int i = 0;  i < objects.size(); ++i) {
    Hit hit = objects[i]->intersect(ray);
    if(hit.t > 0 && (bestHit.t < 0 || hit.t < bestHit.t)) 	bestHit = hit;
  }
  return bestHit;
} 


vec4 trace(Ray ray, int depth) {
	vec4 La(0.9,0.9,0.96,1);
	if (depth > MAX_DEPTH)
		return La;
	Hit hit = firstIntersect(ray);
	if(hit.t < 0) return La; 
	vec4 outRadiance = vec4(0,0,0,0);
	outRadiance = hit.material->ka * La;
	for(unsigned int i = 0;  i < lights.size(); i++){
		vec4 lightNm = (lights[i]->ps - hit.ps).normalize();
		Ray shadowRay(hit.ps + hit.nm * epsilon * sign(dot(hit.nm, ray.nm * -1.0f)), lightNm);
		Hit shadowHit = firstIntersect(shadowRay);
		if(  shadowHit.t < 0  || shadowHit.t > (hit.ps - lights[i]->ps).length() ) 
			outRadiance += hit.material->shade(hit.nm, ray.nm * -1.0f, lightNm, lights[i]->color);
	}

	if(hit.material->isReflective){
		vec4 reflectionDir = hit.material->reflect(ray.nm, hit.nm);
		Ray reflectedRay(hit.ps + hit.nm * epsilon * sign(dot(hit.nm, ray.nm * -1.0f)), reflectionDir);
		outRadiance += trace(reflectedRay, depth+1) * hit.material->Fresnel(ray.nm, hit.nm);
	}
	if(hit.material->isRefractive) { 
		vec4 refractionDir = hit.material->refract(ray.nm, hit.nm);
		Ray refractedRay(hit.ps - hit.nm * epsilon * sign(dot(hit.nm, ray.nm * -1.0f)), refractionDir);
		outRadiance += trace(refractedRay, depth+1) * (vec4(1.0f, 1.0f, 1.0f, 1.0f) - hit.material->Fresnel(ray.nm, hit.nm));
	}

  return outRadiance;
}


void trace_thread(int x1, int y1, int wWidth, int wHeight, int depth, Camera* camera, vec4*  background) {
    for (unsigned int x = x1; x < wWidth; x++)
		    for (unsigned int y = y1; y < y1+wHeight; y++)
                background[y * wWidth + x] = trace(camera->getRay(x,y), 0);
}


void onInitialization() {
	glViewport(0, 0, windowWidth, windowHeight);

	static vec4 background[windowWidth * windowHeight];

	Camera camera(vec4(6,-10,10,1), 
					vec4(-0.2,1,-0.2,1).normalize(),
					vec4(-0.2,1,-0.2,1).cross(vec4(1,0.2,0,1)).normalize()*-1.0f,	
					vec4(1,0.2,0,1));

	Light light1;
	light1.ps = vec4(0,18,30,1);
	light1.color = vec4(1.5,1.5,1.5,1.5);
	lights.push_back(&light1);

	Material greenGlass(vec4(1.0f, 1.0, 1.0, 1), vec4(1.3, 8., 1.3, 1));
	greenGlass.ka = vec4(0.0, 0.2, 0.0, 1);
	greenGlass.kd = vec4(0.0, 0.2, 0.0, 1);
	greenGlass.ks = vec4(0.0, 0.0, 0.0, 1);
	greenGlass.isRefractive = true;
	greenGlass.isReflective = true;
	greenGlass.shininess = 10;

	Material gold(vec4(0.1f, 0.35f, 1.5f, 1), vec4(3.1f, 2.7f, 1.9f,1));
	gold.ka = vec4(0.24f, 0.19f, 0.07f, 1);
	gold.kd = vec4(0.24f, 0.19f, 0.07f, 1);
	gold.ks = vec4(0.1, 0.1, 0.1, 1);
	gold.isRefractive = false;
	gold.isReflective = true;
	gold.shininess = 100;

	Material silver(vec4(0.14f, 0.16f, 0.13f, 1), vec4(4.1f, 2.3f, 3.1, 1));
	silver.ka = vec4(0.19f, 0.19f, 0.19f);
	silver.kd = vec4(0.19f, 0.19f, 0.19f);
	silver.ks = vec4(0.19f, 0.19f, 0.19f);
	silver.isRefractive = false;
	silver.isReflective = true;
	silver.shininess = 100;

	Material glass(vec4(1.0f, 1, 1, 1), vec4(1.3 , 1.3 , 1.3, 1));
	glass.ka = vec4(0.0, 0.0, 0.0, 1);
	glass.kd = vec4(0.0, 0.0, 0.0, 1);
	glass.ks = vec4(0.0, 0.0, 0.0, 1);
	glass.isRefractive = true;
	glass.isReflective = true;
	glass.shininess = 10;

	Cuboid cube(vec4(0,35,-2,0),vec4(0,0,1,0), vec4(1,-1,0,0), 4,4,10, &gold);
	Sphere glassSphere(vec4(2,15,4,0), 2, &glass);
	Sphere silverSphere(vec4(-1,20,4,0), 2, &silver);
	Sphere glassSphere2(vec4(4,10,4,0), 1, &greenGlass);


	objects.push_back(&cube);
	objects.push_back(&glassSphere);
	objects.push_back(&silverSphere);
	objects.push_back(&glassSphere2);

    thread *tt = new thread[NUM_THREADS];
    int steps = windowHeight / NUM_THREADS;
    
	for (int i = 0; i<NUM_THREADS; i++) {
        tt[i] = thread(trace_thread, 0, steps*i, windowWidth, steps, 0, &camera, background);
	}   

    for (unsigned int k=0; k<NUM_THREADS; k++){
        tt[k].join();                
    }

    delete [] tt;

	fullScreenTexturedQuad.Create( background );

	// Create vertex shader from string
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	if (!vertexShader) {
		printf("Error in vertex shader creation\n");
		exit(1);
	}
	glShaderSource(vertexShader, 1, &vertexSource, NULL);
	glCompileShader(vertexShader);
	checkShader(vertexShader, "Vertex shader error");

	// Create fragment shader from string
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	if (!fragmentShader) {
		printf("Error in fragment shader creation\n");
		exit(1);
	}
	glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
	glCompileShader(fragmentShader);
	checkShader(fragmentShader, "Fragment shader error");

	// Attach shaders to a single program
	shaderProgram = glCreateProgram();
	if (!shaderProgram) {
		printf("Error in shader program creation\n");
		exit(1);
	}
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);

	// Connect Attrib Arrays to input variables of the vertex shader
	glBindAttribLocation(shaderProgram, 0, "vertexPosition"); // vertexPosition gets values from Attrib Array 0

	// Connect the fragmentColor to the frame buffer memory
	glBindFragDataLocation(shaderProgram, 0, "fragmentColor");	// fragmentColor goes to the frame buffer memory

	// program packaging
	glLinkProgram(shaderProgram);
	checkLinking(shaderProgram);
	// make this program run
	glUseProgram(shaderProgram);
}

void onExit() {
	glDeleteProgram(shaderProgram);
	printf("exit");
}

// Window has become invalid: Redraw
void onDisplay() {
	glClearColor(0, 0, 0, 0);							// background color 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the screen
	fullScreenTexturedQuad.Draw();
	glutSwapBuffers();									// exchange the two buffers
}

// Key of ASCII code pressed
void onKeyboard(unsigned char key, int pX, int pY) {
	if (key == 'd') glutPostRedisplay();         // if d, invalidate display, i.e. redraw
}

// Key of ASCII code released
void onKeyboardUp(unsigned char key, int pX, int pY) {

}

// Mouse click event
void onMouse(int button, int state, int pX, int pY) {
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {  // GLUT_LEFT_BUTTON / GLUT_RIGHT_BUTTON and GLUT_DOWN / GLUT_UP
	}
}

// Move mouse with key pressed
void onMouseMotion(int pX, int pY) {
}

// Idle event indicating that some time elapsed: do animation here
void onIdle() {
	long time = glutGet(GLUT_ELAPSED_TIME); // elapsed time since the start of the program
}

// Idaig modosithatod...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

int main(int argc, char * argv[]) {
	glutInit(&argc, argv);
#if !defined(__APPLE__)
	glutInitContextVersion(majorVersion, minorVersion);
#endif
	glutInitWindowSize(windowWidth, windowHeight);				// Application window is initially of resolution 600x600
	glutInitWindowPosition(100, 100);							// Relative location of the application window
#if defined(__APPLE__)
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_3_2_CORE_PROFILE);  // 8 bit R,G,B,A + double buffer + depth buffer
#else
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#endif
	glutCreateWindow(argv[0]);

#if !defined(__APPLE__)
	glewExperimental = true;	// magic
	glewInit();
#endif

	printf("GL Vendor    : %s\n", glGetString(GL_VENDOR));
	printf("GL Renderer  : %s\n", glGetString(GL_RENDERER));
	printf("GL Version (string)  : %s\n", glGetString(GL_VERSION));
	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);
	printf("GL Version (integer) : %d.%d\n", majorVersion, minorVersion);
	printf("GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	onInitialization();

	glutDisplayFunc(onDisplay);                // Register event handlers
	glutMouseFunc(onMouse);
	glutIdleFunc(onIdle);
	glutKeyboardFunc(onKeyboard);
	glutKeyboardUpFunc(onKeyboardUp);
	glutMotionFunc(onMouseMotion);

	glutMainLoop();
	onExit();
	return 1;
}
