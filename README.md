# Toy example of a simple raytracer

![Alt text](render.png?raw=true "render image")

## Requirement
1. c++ 11
2. glew
3. opengl

To install glew on ubuntu, run:
`sudo apt install libglew-dev`

## How to run
1. `chmod +x ./run.sh`
2. `./run.sh`
